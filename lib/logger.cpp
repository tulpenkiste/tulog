/*
Tulog is a library for logging to files.
Copyright (C) 2024 - Tulpenkiste

This file is a part of Tulog.
Tulog is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation using version 3 of the License only.

Tulog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Tulog. If not, see <https://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <cassert>
#include <chrono>
#include <filesystem>
#include <iomanip>
#include <iostream>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#ifndef NOFMT
#include <fmt/format.h>
#endif

#include <tulog/logger.hpp>
#include <tulog/oformat.hpp>
#include <tulog/formattype.hpp>
#include <tulog/logtype.hpp>

namespace tulog {
#ifndef NOFMT
	std::string formatString(std::string subject, std::string search, std::vector<std::string> replace) {
		std::string replacementText = replace[0];
		size_t index = 0, positionInString = 0;

		while ((positionInString = subject.find(search, positionInString)) != std::string::npos) {
			subject.replace(positionInString, search.length(), replacementText);
			positionInString += search.length();
			index++;

			if (index > search.size() - 1) break;
			else replacementText = replace[index];
		}

		return subject;
	}
#endif

	const std::vector<FormatType> LOG_FORMAT_TEXTS {
		// TODO: another format
		{FormatValueDescriptor::CallerTypeMessage, "{} | {} : {}"},
		{FormatValueDescriptor::DateCallerTypeMessage, "[{} | {}] {} : {}"},
		{FormatValueDescriptor::TypeCallerDateMessage, "[ {} ] {} @ {} > {}"}
	};

	const std::vector<std::string> TYPE_TEXTS {
		"   Debug   ",
		"Information",
		"  Warning  ",
		"   Error   ",
		"   Fatal   "
	};

	Logger::Logger(std::filesystem::path target, int format, unsigned int maximumLogFiles, bool useRedirectMethod) : mFormat((const OFormat)format), mRedirect(useRedirectMethod) {
		// Enforced minimum is 1
		if (maximumLogFiles == 0)
			maximumLogFiles = 1;
		
		std::string extension;

		switch (format) {
			// Simple Tulog Format
			case SIMPLE:
				extension = ".tlslog";
				break;
			// Date Tulog Format
			case DATE:
				extension = ".tldlog";
				break;
			// Kobold Report Format (default)
			case KORO:
				extension = ".korolog";
				break;
			default:
				extension = ".log";
				break;
		}

		if (std::filesystem::is_directory(target)) {
			std::vector<std::filesystem::path> sorted {(size_t)maximumLogFiles};

			for (int i = 0; i < 5; i ++) {
				sorted[i] = std::filesystem::path {};
			}

			for (const auto& dirEntry : std::filesystem::directory_iterator(target)) {
				if (dirEntry.path().empty() || std::filesystem::is_directory(dirEntry)) continue;
				if (dirEntry.path().extension().string() != extension) continue;

				std::string curPath = dirEntry.path().filename().string();
				std::string ext;
				size_t lastdot = curPath.find_last_of(".");

				if (lastdot != std::string::npos) {
					ext = curPath.substr(lastdot, curPath.npos);
					curPath = curPath.substr(0, lastdot);
				} else {
					ext = "";
				}

				if (std::all_of(curPath.begin(), curPath.end(), isdigit)) {
					int pathAsInt = std::stoi(curPath);

					sorted[(maximumLogFiles - 1) - pathAsInt] = dirEntry;
				}
			}

			for (const auto& dirEntry : sorted) {
				if (dirEntry.empty()) continue;

				size_t lastdot = dirEntry.filename().string().find_last_of(".");
				std::string curPath = dirEntry.filename().string().substr(0, lastdot);

				if (std::all_of(curPath.begin(), curPath.end(), isdigit)) {
					int pathAsInt = std::stoi(curPath);
					int nextPath = pathAsInt + 1;

					if (nextPath == maximumLogFiles) std::filesystem::remove(dirEntry);
					else {
						std::string newPath = formatString(dirEntry.string(), std::to_string(pathAsInt), {std::to_string(nextPath)});

						const std::filesystem::path renamePath { newPath };

						std::filesystem::rename(dirEntry, renamePath);
					}
				}
			}
		} else {
			std::filesystem::create_directory(target);
		}

		mOutput.open(target.string() + "/0" + extension, std::ios::out);
		assert(mOutput.is_open());

		if (mRedirect) {
			mOriginalCout = std::cout.rdbuf();
			mOriginalCin = std::cin.rdbuf();
			mOriginalCerr = std::cerr.rdbuf();
			std::streambuf* mRedirBuff = mOutput.rdbuf();

			std::cout.rdbuf(mRedirBuff);
			std::cin.rdbuf(mRedirBuff);
			std::cerr.rdbuf(mRedirBuff);
		}
	}

	Logger::~Logger() {
		if (mRedirect) {
			std::cout.rdbuf(mOriginalCout);
			std::cin.rdbuf(mOriginalCin);
			std::cerr.rdbuf(mOriginalCerr);
		}

		mOutput.close();
	}

	void Logger::log(int type, std::string caller, std::string message) {
		bool doError = type > tulog::WARN;
		const std::string strType = TYPE_TEXTS[type];

		std::string toOut = internalFormat(strType, caller, message);

		internalLog(toOut, doError);
	}

	void Logger::log(std::string typeStr, std::string caller, std::string message, bool doError) {
		std::string toOut = internalFormat(typeStr, caller, message);

		internalLog(toOut, doError);
	}

	std::string Logger::internalFormat(const std::string& type, const std::string& caller, const std::string& message) {
		const std::string format = LOG_FORMAT_TEXTS[mFormat].format;
		std::string toOut = "";

		std::string time;

		{
			auto curTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
			std::stringstream stringStream;
			stringStream << std::put_time(std::localtime(&curTime), "%Y/%m/%d %H:%M:%S");
			time = stringStream.str();
		}

		switch (LOG_FORMAT_TEXTS[mFormat].type) {
			case CallerTypeMessage:
				toOut +=
				#ifndef NOFMT
				fmt::format(format, caller, type, message)
				#else
				formatString(format, "{}", {caller, strType, message})
				#endif
				+ "\n";
				break;
			case DateCallerTypeMessage:
				toOut +=
				#ifndef NOFMT
				fmt::format(format, time, caller, type, message)
				#else
				formatString(format, "{}", {time, caller, strType, message})
				#endif
				+ "\n";
				break;
			case TypeCallerDateMessage:
				toOut +=
				#ifndef NOFMT
				fmt::format(format, type, caller, time, message)
				#else
				formatString(format, "{}", {strType, caller, time, message})
				#endif
				+ "\n";
				break;
			default:
				break;
		}

		return toOut;
	}

	void Logger::internalLog(const std::string& text, bool isErr) {
		if (!mRedirect) mOutput << text;

		if (!isErr) std::cout << text;
		else std::cerr << text;
	}
}