/*
Tulog is a library for logging to files.
Copyright (C) 2024 - Tulpenkiste

This file is a part of Tulog.
Tulog is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation using version 3 of the License only.

Tulog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Tulog. If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>

#include <tulog/logger.hpp>
#include <tulog/oformat.hpp>
#include <tulog/logtype.hpp>

int main(void) {
	for (int i = 0; i <= tulog::OFormat::KORO; i++) {
		tulog::Logger logger { std::filesystem::path {(std::string)(getenv("HOME")) + "/.local/share/tulogtest"}, i };

		logger.log(tulog::INFORMATION, "tulogtest", "test");
		logger.log(tulog::WARN, "tulogtest", "test");
		logger.log(tulog::ERROR, "tulogtest", "test");
		logger.log(tulog::FATAL, "tulogtest", "test");
	}

	for (int i = 0; i <= tulog::OFormat::KORO; i++) {
		tulog::Logger logger { std::filesystem::path {(std::string)(getenv("HOME")) + "/.local/share/tulogtest"}, i, true };

		logger.log(tulog::INFORMATION, "tulogtest", "test (redirectMethod)");
		logger.log(tulog::WARN, "tulogtest", "test (redirectMethod)");
		logger.log(tulog::ERROR, "tulogtest", "test (redirectMethod)");
		logger.log(tulog::FATAL, "tulogtest", "test (redirectMethod)");
	}

	return 0;
}
