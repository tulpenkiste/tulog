/*
Tulog is a library for logging to files.
Copyright (C) 2024 - Tulpenkiste

This file is a part of Tulog.
Tulog is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation using version 3 of the License only.

Tulog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Tulog. If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

namespace tulog {
	enum OFormat {
		SIMPLE,
		DATE,
		KORO
	};
}
