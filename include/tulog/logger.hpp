/*
Tulog is a library for logging to files.
Copyright (C) 2024 - Tulpenkiste

This file is a part of Tulog.
Tulog is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation using version 3 of the License only.

Tulog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Tulog. If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <fstream>
#include <filesystem>
#include <string>

#include "logtype.hpp"
#include "oformat.hpp"

namespace tulog {
	class Logger {
	public:
		Logger(std::filesystem::path target, int format = OFormat::KORO, unsigned int maximumLogFiles = 5, bool useRedirectMethod = false);
		~Logger();

		void log(int type, std::string caller, std::string message);
		void log(std::string typeText, std::string caller, std::string message, bool isError = false);
	private:
		std::string internalFormat(const std::string& type, const std::string& caller, const std::string& message);
		void internalLog(const std::string& isText, bool isError = false);
		const OFormat mFormat;
		const bool mRedirect;

		std::fstream mOutput;

		std::streambuf* mOriginalCout;
		std::streambuf* mOriginalCin;
		std::streambuf* mOriginalCerr;
	};
}
